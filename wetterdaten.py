#function to get weatherdata from the provinz
import requests
from io import StringIO
import numpy as np
import pandas as pd

def wetterdaten(station='83200MS', sensor='GS', start='20210101', end='20211231'):
    """
    Download weather data from Provinz BZ weather service

    Args:
        station (str, required): The weather station to download the data from. Defaults to '83200MS' (Bolzano/Bozen).
        sensor (str, required):  The sensor for which to download data. Check online the API documentation for a list 
                                 of sensors. Defaults to 'GS' (Globalstrahlung == GHI).
        start (str, optional): Start date, formatted as 'YYYYMMDD'. Defaults to '20210101'.
        end (str, optional): End date, formatted as 'YYYYMMDD'. Defaults to '20211231'.

    Returns:
        pd.DataFrame: Pandas dataframe with a datetime index and the desired sensor measurement values. 
    """

    url='http://daten.buergernetz.bz.it/services/meteo/v1/timeseries?station_code={station}&output_format=CSV&sensor_code={sensor}&date_from={start}&date_to={end}'

    furl = url.format(station=station, sensor=sensor, start=start, end=end)
    result = requests.get(furl)

    ffile = StringIO(result.text)
    df = pd.read_csv(ffile, index_col='DATE', parse_dates=False,).sort_index()
    df['VALUE'] = df.VALUE.astype(np.float32)
    df['date'] = np.nan

    for i in df.index:
        df.loc[i, 'date'] = i.replace('CET', '').replace('CEST', '').replace('T', ' ')

    df.index = pd.to_datetime(df.date, utc=False)

    df.drop(columns=['date'], inplace=True)

    if sensor == 'GS':
        df = df.rename(columns={
            'VALUE': 'ghi'
        })
    
    return df.tz_localize('Europe/Rome')

df = wetterdaten(end='20210102')
