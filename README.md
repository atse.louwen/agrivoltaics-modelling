# AgriVoltaics Modelling

## Introduction
In this repository you can model bifacial irradiance using PVlib. It is mostly based on this link: https://pvlib-python.readthedocs.io/en/stable/gallery/bifacial/plot_pvfactors_fixed_tilt.html for fixed tilt modelling. 

To implement tracking, you can use the same general procedure but look here for the tracking part: 
https://pvlib-python.readthedocs.io/en/stable/gallery/bifacial/plot_bifi_model_pvwatts.html

## Files
* BifacialFixedTilt.ipynb:  the main notebook which walks through the procedure
* requirements.txt:         a file which shows the needed python packages
* wetterdaten.py:           a file with a function to download weatherdata from the province of Bolzano
* .gitignore:               a file that configures which folders not to sync to/from gitlab
